Om LIPID
========

Protokoll utviklet av Dan R. Johansen ved Nasjonalt Kompetansesenter for Familiær Hyperkolesterolemi i samarbeid med eget fagmiljø på OUS og DIPS i Bergen.

Mapper
======

* **Documents** - innholder dokumenter som ikke er brukt direkte av systemet.
* **Images** - Inneholder bilder som inngår i dokumentasjon.
* **Patient** - Rapporter (\*.fr3) og oversiktsbilde (\*.html) for en enkeltpasient, samt PDF-filer som brukes av systemet og notatmaler (\*.note).
* **Population** - Rapporter (*.fr3) som inneholder flere pasienter.


Forsiden
========

I roten ligger filen [index.html](http://downloads.fasttrak.no/LIPID/index.html).  Dette er forsiden som viser for brukerne av FastTrak.
Websidene ligger på [nedlastingsserveren](http://downloads.fasttrak.no) og oppdateres automatisk hver gang det gjøres en push mot master på bitbucket.

Nedlasting
==========

Forvaltere kan laste ned ukentlige oppdateringer av metadata, og kan også få tilsendt en epost med instruksjoner og lenke til nedlasting av zip-fil. 
Ta kontakt med DIPS Brukerstøtte for mer informasjon.


Installasjon
============

Pakk ut nedlastet zip-fil og kopier inn med overskriving av eksisterende filer. 
Kjør deretter `\bin\FastTrakUpdate.exe` og kryss av for alle tabeller før kjøring. 
Husk å krysse av for "Lokale filer".

Bergen, 15. november 2017  
Magne Rekdal,  
Medisinsk Rådgiver